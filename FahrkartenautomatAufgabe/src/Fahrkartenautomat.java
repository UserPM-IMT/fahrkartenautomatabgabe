import java.util.*;

class Fahrkartenautomat{    
    
	
	public static double convertCentToEuro(int cent) {
        double euro = cent;
        euro = euro / 100;
        return euro;
    }
    
    
    public static int captureTicketOrder() {
        Scanner tickets = new Scanner(System.in);
        String fahrkarte[] = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",                 "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
        int kartenpreisCent[] = {290, 330, 360, 190, 860, 900, 960, 2350, 2430, 2490};
        int arrayLength = kartenpreisCent.length;
        int zuZahlenderBetragCent = 0;
        int ticketArt = 0;
        int eingabe = 0;
    
        System.out.printf("Fahrkartenbestellvorgang:\n");
        System.out.printf("=========================\n\n");

 
        while(ticketArt != arrayLength+1) {
            System.out.printf(" W�hlen Sie :\n");
            int i = 0;
            for(i = 0; i < arrayLength; i++) {
            System.out.printf("%8s %-35s %7.2f �\n", "("+(i+1)+")", fahrkarte[i], convertCentToEuro(kartenpreisCent[i]));
            }
            System.out.printf("\n%8s Bezahlen\n", "("+(arrayLength+1)+")");
            System.out.printf("\n Ihre Wahl : ");
            try {ticketArt = tickets.nextInt();} 
            catch(InputMismatchException e) {
                String error = tickets.next();
                System.out.printf("\n" + error + " ist keine g�ltige Eingabe\n");
                ticketArt = 0;
                eingabe = -1;
            }
            System.out.printf("\n\n");
            if(eingabe != -1) {
                if(ticketArt > 0 && ticketArt < arrayLength+1) {
                    System.out.printf("Wie viele Tickets m�chten Sie kaufen : ");
                    int anzahlTickets = tickets.nextInt();
                    System.out.printf("\n\n");
                    zuZahlenderBetragCent += (anzahlTickets*kartenpreisCent[ticketArt-1]);
                } else if(ticketArt == arrayLength+1) {} 
                
                else {    System.out.printf(ticketArt + " ist keine g�ltige Eingabe\n\n\n");}} 
            
            else {eingabe = 0;}
        }
        
        return zuZahlenderBetragCent;
    }    
    
    
    public static int fahrkartenBezahlen(int zuZahlenderBetragCent) {
        int eingezahlterGesamtbetragCent = 000;
        double eingezahlterGesamtbetrag = 0;
        double zuZahlenderBetrag = convertCentToEuro(zuZahlenderBetragCent);
        double eingeworfeneM�nze;
        int eingeworfeneM�nzeCent;
        Scanner tastatur = new Scanner(System.in);
        
        while(eingezahlterGesamtbetragCent < zuZahlenderBetragCent)
        {
        	System.out.printf("Noch zu zahlen: " + "%.2f" + " Euro\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
        	System.out.print("Eingabe (mind. 1Ct, h�chstens 10 Euro): ");
            eingeworfeneM�nze = tastatur.nextDouble();
            eingeworfeneM�nzeCent = (int)(eingeworfeneM�nze * 100);
            eingezahlterGesamtbetragCent += eingeworfeneM�nzeCent;
            zuZahlenderBetrag -= eingeworfeneM�nze;
            }
        return eingezahlterGesamtbetragCent;
    }
    
    
    public static void spendingTickets() {
        System.out.println("\nFahrschein wird ausgegeben");
           for (int i = 0; i < 8; i++) {
              System.out.print("=");
              wait(200);
           }
           System.out.println("\n\n");
    }
    
    
    public static void spendingChange(int eingezahlterGesamtbetragCent, int zuZahlenderBetragCent) {
        int r�ckgabebetragCent = eingezahlterGesamtbetragCent - zuZahlenderBetragCent;
        double r�ckgabebetrag = r�ckgabebetragCent;
        r�ckgabebetrag = r�ckgabebetrag / 100;
        
        if(r�ckgabebetragCent > 000) {
           System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f" + " EURO\n", r�ckgabebetrag);
           System.out.println("wird in folgenden M�nzen ausgezahlt:");
           wait(800);
           // 2 EURO-M�nzen
           while(r�ckgabebetragCent >= 200) {r�ckgabebetragCent = dispenseCoins(200, r�ckgabebetragCent);}

           // 1 EURO-M�nzen
           while(r�ckgabebetragCent >= 100) {r�ckgabebetragCent = dispenseCoins(100, r�ckgabebetragCent);}
 
           // 50 CENT-M�nzen
           while(r�ckgabebetragCent >= 50) {r�ckgabebetragCent = dispenseCoins(50, r�ckgabebetragCent);}
 
           // 20 CENT-M�nzen
           while(r�ckgabebetragCent >= 20) {r�ckgabebetragCent = dispenseCoins(20, r�ckgabebetragCent);}
 
           // 10 CENT-M�nzen
           while(r�ckgabebetragCent >= 10) {r�ckgabebetragCent = dispenseCoins(10, r�ckgabebetragCent);}
 
           // 5 CENT-M�nzen
           while(r�ckgabebetragCent >= 5) {r�ckgabebetragCent = dispenseCoins(5, r�ckgabebetragCent);}
 
           // 2 CENT-M�nzen
           while(r�ckgabebetragCent >= 2) {r�ckgabebetragCent = dispenseCoins(2, r�ckgabebetragCent);}

           // 1 CENT-M�nzen
           while(r�ckgabebetragCent >= 1) {r�ckgabebetragCent = dispenseCoins(1, r�ckgabebetragCent);}
        }   
    }
    
    public static int dispenseCoins(int i, int r�ckgeld) {
        if(i >= 100) {
            System.out.printf("%d EURO\n", i / 100);
            r�ckgeld -= i;
        } else {
            System.out.printf("%d CENT\n", i);
            r�ckgeld -= i;
        }
        wait(500);
        return r�ckgeld;
    }
    
    
    public static void wait(int millisekunde) {
        try {Thread.sleep(millisekunde);}
        catch (InterruptedException e) {e.printStackTrace();}
    }
    
    
    public static void main(String[] args)
    {
        while(true == true) {
            int gesamtBetrag = captureTicketOrder();
            int gesamtEingezahlt = fahrkartenBezahlen(gesamtBetrag);
            spendingTickets();
            spendingChange(gesamtEingezahlt, gesamtBetrag);
            wait(1000);
            System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir w�nschen Ihnen eine gute Fahrt.");
            System.out.printf("========\n\n");
            wait(5000);
        }
    }
}